const faker = require("faker");

module.exports = (Movie, quantity) => {
  for (var i = 0; i < quantity; i++) {
    Movie.create({
      name: faker.name.findName(),
      director: faker.name.findName(),
      year: faker.date.past(),
      description: faker.hacker.phrase(),
      gender: faker.commerce.productAdjective(),
      imgProfile: faker.random.image()
    });
  }
};
