module.exports = (sequelize, type) => {
  return sequelize.define("movies", {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: type.STRING(180),
      unique: {
        args: true,
        msg: "Ya existe una pelicula con ese titulo.",
        fields: ["name"]
      }
    },
    director: {
      type: type.STRING(150)
    },
    year: {
      type: type.DATE
    },
    description: { type: type.TEXT },
    gender: { type: type.STRING(25)},
    imgProfile: type.STRING(255)
  });
};
