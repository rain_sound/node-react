const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const router = require("./routes");
const config = require("./config");

const app = express();

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(cors());
app.use(router);

//Server
app.listen(config.port, () => {
  console.log(`Server running on localhost:${config.port}`);
});

module.exports = app;
